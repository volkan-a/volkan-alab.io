---
layout: post
title: ESM4007 - Güç sistemleri final sınavı çözümleri
math: true
comments: true
date: 2019-01-18
---
### Sorular

> **Soru 1:** Aşağıda verilen güç/soğutma çevriminde R134a gazı kullanılmaktadır. $90^\circ C$’e doymuş buhar Boiler’dan çıkmakta ve türbinde kondenser basıncına genişletilmektedir. Evaporatör’den $-15^\circ C$’de doymuş buhar çıkmakta ve Kondenser basıncına sıkıştırılmaktadır. İki çevrim arasındaki debi oranı türbin gücü kompresöre yetecek kadar seçilmiştir. İki çıkış akımı karışmakta ve kondenseri $45^\circ C$’de terketmektedir. İki çevrimin debi oranını ve $\dot{Q}_L/\dot{Q}_H$ oranını hesaplayınız.
> 
>![soru 1]({{ site.url }}/img/exam1.png ){:height="300px"}
<!--more-->

* Kondenser çıkışı
  
  $$
  \color{green}{T_1 = 45^\circ C}, \quad \color{green}{x_1 = 0.0} \\
  \color{red}{P_1 = 1.15 MPa}, \quad  
  \color{red}{h_1=263.7 kJ/kg}, \quad
  \color{red}{s_1=1.213 kJ/kg}
  $$

* Türbin girişi
  
  $$
  \color{green}{T_2=90^\circ C}, \quad
  \color{green}{x_2=1.0}\\
  \color{red}{P_2=3.23 MPa}, \quad
  \color{red}{h_2=425.5 kJ/kg}, \quad
  \color{red}{s_2=1.667 kJ/kg}
  $$

* Kompresör girişi
  
  $$
  \color{green}{T_3=-15^\circ C}, \quad
  \color{green}{x_3=1.0}\\
  \color{red}{P_3=163 kPa}, \quad
  \color{red}{h_3=389.5 kJ/kg}, \quad
  \color{red}{s_3=1.737 kJ/kg}
  $$

* Türbin çıkışı

$$
\color{green}{P_4=P_1}, \quad
\color{green}{s_4=s_2} \\
\color{red}{h_4=407.9 kJ/kg}
$$

* Kompresör çıkışı

$$
\color{green}{P_5=P_1}, \quad
\color{green}{s_5=s_3} \\
\color{red}{h_5=430.4 kJ/kg}
$$

* Pompa çıkışı
  
  $$
  \color{green}{P_6=P_2}, \quad
  \color{green}{s_6=s_1} \\  
  \color{red}{h_6=265.5 kJ/kg}
  $$

* Genleşme valfi çıkışı
  
  $$
  \color{green}{P_7=P_3}, \quad
  \color{green}{h_7=h_1}
  $$

* Türbin ve kompresör güçleri denk ise

$$
\dot{m}_2(h_2-h_4)=\dot{m}_3(h_5-h_3) \\[10pt]
\frac{\dot{m}_2}{\dot{m}_3}=\frac{430.4-389.5}{425.5-407.9}=2.32
$$

* Çevrimin performansı ise
  
  $$
  \frac{\dot{Q}_L}{\dot{Q}_H} =\frac{\dot{m}_3}{\dot{m}_2}\frac{h_2-h_6}{h_3-h_7}=\frac{1}{2.32}\frac{389.5-263.7}{425.5-265.5}=0.339
  $$

> **Soru 2:** Brayton çevrimi esasına dayanan bir gaz çevrim santralinin net gücü 100 MW’tır. Çevrimin minimum sıcaklığı 300 K, egzoz sıcaklığı 750 K’dir. Minimum basınç 100 kPa ve sıkıştırma oranı 14:1’dir. Kompresör ve türbinin izentropik verimleri sırasıyla %85 ve %88 ise debiyi ve termal verimi hesaplayınız.

$$
c_P \approx 1.0 kJ/kgK\\[10pt]
k=c_p / c_v = 1.4 \\[10pt]
\frac{k-1}{k} \approx 0.286
$$

* Kompresör

$$
T_g=300K,\quad
P_g=100KPa,\quad
\text{PR} = 14 \\[10pt]
\frac{T_{ç,s}}{T_g} = \text{PR}^{0.286} \\[10pt]
T_{ç,s}=638 K \\[10pt]
\eta_{komp}=\frac{T_{ç,s}-T_g}{T_{ç,a}-T_g}=0.85\\[10pt]
T_{ç,a}=697 K
$$

* Türbin

$$
T_{ç,s}=750K \\[10pt]
\frac{T_{g}}{T_{ç,s}} = \text{PR}^{0.286} \\[10pt]
T_{g}=1595 K \\[10pt]
\eta_{\text{turb}}=\frac{T_{g}-T_{ç,a}}{T_{g}-T_{ç,s}}=0.88\\[10pt]
T_{ç,a}=851 K
$$


Debi için:

$$
\dot{W}_{net}=\dot{m}(w_{turb}-w_{komp}) \\[10pt]
=\dot{m}c_p \Big (  (1595-851) - (697-300)\Big )\\[10pt]
\dot{m}=\frac{10\times10^3}{1\times347}=28.8 kg/s
$$


Termal verim:

$$
\dot{Q}_h=\dot{m}c_p\big ( 1595-697\big)\\[10pt]
\dot{Q}_h=25.86 MW \\[10pt]
\eta_{termal}=\frac{\dot{W}_{net}}{\dot{Q}_H}=\frac{10}{25.86}=\%38.6
$$